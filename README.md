# Daily Mealz Task

## Installation and configuration steps

```bash
git clone git@gitlab.com/norhanelnezamy/daily_mealz.git
```
```bash
composer update
```
```bash
cp -i .env.example .env
```
```bash
php artisan key:generate
```

please create a database in your SQL serve

configure your database connection in .env file

```bash
php artisan serve
```
## Usage

Use [Landing](http://127.0.0.1:8000) to landing page.
