<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Host;

class HostsController extends Controller
{

  public function unique_visitors(){
    /*
    * SELECT `URL` FROM HOSTS GROUP BY `URL`;
    * 81982 rows in set (0.71 sec)
    *====================================================================
    * INSERT INTO unique_visitors (ID,URL) SELECT ID, URL FROM HOSTS GROUP BY URL;
    * Query OK, 81982 rows affected (1.53 sec)
    */
    $visitiors = Host::groupBy('URL')->get("URL");
    return $visitiors;
  }

  public function hits(){
    /*
    * SELECT `URL`, COUNT(*) AS  `HITS` FROM HOSTS GROUP BY `URL`;
    * 81982 rows in set (1.09 sec)
    *==================================================================
    * INSERT INTO hits (ID,URL,COUNT) SELECT ID, URL, COUNT(*) AS  `HITS` FROM HOSTS GROUP BY URL;
    * Query OK, 81982 rows affected (2.04 sec)
    */
    $visitiors = Host::groupBy('URL')->select(DB::raw('COUNT(*) as HITS, URL'))->get();
    return $visitiors;
  }

  public function hits_top(){
    /*
    * SELECT `URL`, COUNT(*) AS  HITS FROM HOSTS GROUP BY `URL` ORDER BY HITS DESC;
    * 81982 rows in set (1.75 sec)
    *==================================================================
    * INSERT INTO hits_top (ID,URL,COUNT) SELECT ID, URL, COUNT(*) AS  `HITS` FROM HOSTS GROUP BY URL ORDER BY HITS DESC;
    * Query OK, 81982 rows affected (2.03 sec)
    */
    $visitiors = Host::groupBy('URL')->select(DB::raw('COUNT(*) as HITS, URL'))->orderBy('HITS', 'DESC')->get();
    return $visitiors;
  }


}
